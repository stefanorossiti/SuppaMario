# SuppaMario

This is a concept for a clone of super Mario. 
With the possibility to create your own maps.

![ezgif-2-db46d0ad92.gif](images/ezgif-2-db46d0ad92.gif)

## Description

![Scheme](images/suppamario.png)

You can create you own map modifying the map file:

`\suppaMariov6\build\classes\mappe\map1.txt`

![Scheme](images/suppamario2.png)

- Idle cycle background. Speeding according to mario movement speed
- Mario can move left and right, jump and crouch
- Collide with solid blocks
- Dies when hit by lateral collision with koopa, by deadly blocks (water) or 
  falling out of the map
- Has dead, jump and crouch animation
- Background and death animation music
- The map is loaded from an external file, you can build with:
  - 2 solid block (bricks)
  - 1 pass-through block (clouds)
  - 1 deadly block (water)
- Implemented 2 walking koopa enemies, they die and disappear on upper 
  collision with mario

## Requirements

- Java (6 to) 8 (max, recommended)
- NetBeans (highly recommended)

Java Applets have been dropped with Java 9. So you need an older versione to run this project.
Java 8 is the preferred version. Until Java 6 should be ok.

Problems running the project are expected, applets are deprecated and not supported by modern browsers.**

This is a NetBeans Project. Using NetBeans isn't mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.

[NetBeans Instructions](https://netbeans.apache.org/tutorial/main/kb/docs/java/javase-jdk8/)

Alternatively, you can use IntellijJ IDEA with the [Applet runner plugin](https://plugins.jetbrains.com/plugin/16682-applet-runner) (not tested).

## Installation and run

Clone the repo:
    
```bash
https://gitlab.com/stefanorossiti/SuppaMario.git
```

Run `SuppaMarioApplet.java` with your preferred IDE (NetBeans is recommended) 
using java 8.

## License 

All my work is released under [DBAD](https://www.dbad-license.org/) license.