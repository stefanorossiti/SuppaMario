/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author BBC
 */
public class Animation {

    private ArrayList frames;
    private int currentFrame; //index del frame attivo
    private long animTime; //tempo passato per questo frame
    private long totalDuration; //durata del frame

    //costruttore
    public Animation() {
        frames = new ArrayList();
        totalDuration = 0;

        synchronized (this) {
            animTime = 0;
            currentFrame = 0;
        }
    }

    public synchronized void addFrame(Image image, long duration) {
        totalDuration += duration;
        frames.add(new AnimFrame(image, totalDuration));
    }
    
    public synchronized void reset() {
        this.setCurrentFrame(0);
        this.setAnimTime(0);
    }
    
    public synchronized void update(long elapsedTime) {
        //se ho piu di un frame
        if (frames.size() > 1) {
            //incremento il timer
            animTime += elapsedTime;
            //se ha fatto tutta l animazione
            if (animTime >= totalDuration) {
                //resetto il tempo dell animazione ma tengo conto 
                //del tempo in piu che e passato e imposto il frame corr a 0 
                animTime = animTime % totalDuration;
                currentFrame = 0;
            }

            //se il tempo dell animazione e maggiore della durata
            //del frame vado a quello dopo, controllo ogni frame
            while (animTime > getFrame(currentFrame).endTime) {
                currentFrame++;
            }
        }
    }

    //GETTER
    public synchronized Image getImage() {
        if (frames.isEmpty()) {
            return null;
        } else {
            return getFrame(currentFrame).image;
        }
    }

    private AnimFrame getFrame(int i) {
        return (AnimFrame) frames.get(i);
    }

    public void setFrames(ArrayList frames) {
        this.frames = frames;
    }

    public void setCurrentFrame(int currentFrame) {
        this.currentFrame = currentFrame;
    }

    public void setAnimTime(long animTime) {
        this.animTime = animTime;
    }

    public void setTotalDuration(long totalDuration) {
        this.totalDuration = totalDuration;
    }
    
    //NUOVA CLASSE
    private class AnimFrame {
        Image image;
        long endTime;

        public AnimFrame(Image image, long endTime) {
            this.image = image;
            this.endTime = endTime;
        }
        
        public long getEndTime(){
            return this.endTime;
        }
        
        public void setImg(Image img){
            this.image = img;
        }
    }
}
