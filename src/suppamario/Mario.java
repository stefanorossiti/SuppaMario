/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

import java.awt.Rectangle;

/**
 *
 * @author BBC
 */
public class Mario {
    //costanti

    final int JUMPSPEED = -16;
    final int MOVESPEED = 7;
    //vado a leggere i bgs della classe istanziante 
    private static Background bg1 = SuppaMarioApplet.getBg1();
    private static Background bg2 = SuppaMarioApplet.getBg2();
    //rectangle per le collision
    //collisione testa
    static Rectangle rUpCollision = new Rectangle(0, 0, 0, 0);
    //collisione piedi
    static Rectangle rDownCollision = new Rectangle(0, 0, 0, 0);
    //posizioneato sotto ai piedi serve per vedere se si sta camminando sul vuoto
    static Rectangle rLeftCollision = new Rectangle(0, 0, 0, 0);
    static Rectangle rRightCollision = new Rectangle(0, 0, 0, 0);
    static Rectangle rSurroundingMario = new Rectangle(0, 0, 0, 0);
    //coord centrali di mario (sprite)
    private int centerX;
    private int centerY;
    private int jumpX; //ogni volta che mario tocca il suolo viene memorizzata la posizione in x e y in modo che
    private int jumpY; //se muore viene rispristinata la sua ultima posizioneda vivo
    //variabili di controllo movimento
    private boolean movingLeft = false;
    private boolean movingRight = false;
    private boolean ducked = false;
    private boolean dead = false;
    //dienta true se sta saltando
    private boolean jumped = false;
    private boolean turnedRight = true;
    private int speedX;
    private int speedY;
    private boolean cantMooveLeft;
    private boolean cantMooveRight;
    private boolean cantFall;

    public Mario(int x, int y) {
        this.centerX = x;
        this.centerY = y;
        this.jumped = false;
        this.cantFall = false;
        this.speedX = 0;
        this.speedY = 0;
    }

    //questo metodo sposta il bg  o mario in base alle velocita
    public void update() {
        //se mario non e morto esegue lo spostamento normale, se no lo spostamento di morte
        if (!this.dead) {
            //sposto in x
            if (speedX < 0 && !cantMooveLeft) {
                // evito di andare troppo a sx
                if (centerX + speedX >= 100) {
                    //se la velocita e negativa vado a sx
                    centerX += speedX;
                } else {
                    //se sono troppo a dx muovo solo il bg
                    bg1.moveLeft();
                    bg2.moveLeft();
                }
            } else if (speedX > 0 && !cantMooveRight) {
                //se la velocita e positiva controllo dove sono
                if (centerX <= 500) {
                    //se non sono troppo a destra sposto mario a dx
                    centerX += speedX;
                } else {
                    //se sono troppo a dx muovo solo il bg
                    bg1.moveRight();
                    bg2.moveRight();
                }
            }

            //se puo scendere scende
            //aumento di uno la velocita in y
            if(!cantFall || jumped){
                speedY += 1; //decellero fino a portarlo a terra  
                centerY += speedY;
            }
            //aggiorno le coordinate delle collisioni
            rSurroundingMario.setBounds(this.getCenterX() - 22, this.getCenterY() - 19, 44, 37);
            rUpCollision.setBounds(this.getCenterX() - 5, this.getCenterY() - 14, 10, 1); //spessore fine solo per la collisione superiore
            rDownCollision.setBounds(this.getCenterX() - 13, this.getCenterY() + 16, 26, 1); //
            rLeftCollision.setBounds(this.getCenterX() - 17, this.getCenterY(), 1, 1); //
            rRightCollision.setBounds(this.getCenterX() + 17, this.getCenterY(), 1, 1); //

            //controlla se e cadut troppo in basso da morire
            if (this.centerY > 480) {
                die();
            }

            cantMooveLeft = false;
            cantMooveRight = false;
            cantFall = false;
            
            //altrimenti se e morto
        } else {
            speedY += 1;
            centerY += speedY;
        }
    }

    void revive() {
        this.centerX = this.jumpX;
        this.centerY = this.jumpY;
        this.speedX = 0;
        this.speedY = 0;

        this.dead = false;
    }

    void saveCheckPoint() {
        //se mario non e morto salvo le coord di rinascita per quando muore
        if (!this.dead && this.speedY == 0) {
            if (movingLeft) {
                this.jumpX = this.centerX + 20;
            } else if (movingRight) {
                this.jumpX = this.centerX - 20;
            } else {
                this.jumpX = this.centerX;
            }

            this.jumpY = this.centerY - 10;
        }
    }

    void stop() {
        speedX = 0;
        bg1.moveStop();
        bg2.moveStop();
        movingLeft = false;
        movingRight = false;
    }

    //se il salto sta ancora andando verso l alto lo fermo prematuramente
    void stopJumping() {
        if (speedY < 0) {
            speedY = 0;
            jumped = false;
        }
    }

    //METODI GENERALI
    public void moveRight() {
        if (!ducked && !movingLeft && !cantMooveRight) {
            speedX = MOVESPEED;
            movingLeft = false;
            movingRight = true;
            turnedRight = true;
            cantMooveLeft = false;
        }
    }

    public void moveLeft() {
        if (!ducked && !movingRight && !cantMooveLeft) {
            speedX = -MOVESPEED;
            movingLeft = true;
            movingRight = false;
            turnedRight = false;
            cantMooveRight = false;
        }
    }

    public void jump() {
        //se e vivo salta
        if (!this.dead && !this.jumped) {
            speedY = JUMPSPEED;
            jumped = true;
            ducked = false;
        }
    }

    void upCollisionDetected() {
        this.speedY = 0;
        //dopo aver azzerato la velocita allora imposto la posizione di mario e del suo rettangolo sup appena sotto al tile colliso
        rUpCollision.setLocation((int) rUpCollision.getLocation().getX(), (int) (rUpCollision.getLocation().getY() - (rUpCollision.getLocation().getY() % 32) + 32));
        this.centerY = (int) rUpCollision.getLocation().getY() + 16;
        //METTERE Y SOTTO IL BLOCCO
    }

    void downCollisionDetected() {
        //dopo aver azzerato la velocita allora imposto la posizione di mario e del suo rettangolo inf appena sotto al tile colliso
        //esegue solo se sta salendo o salendo
        rUpCollision.setLocation((int) rUpCollision.getLocation().getX(), (int) (rUpCollision.getLocation().getY() - (rUpCollision.getLocation().getY() % 32)));
        this.centerY = (int) rUpCollision.getLocation().getY() + 16;
        this.speedY = 0;
        this.jumped = false;
        this.cantFall = true;

        saveCheckPoint();
    }

    void leftCollisionDetected() {
        cantMooveLeft = true;
        //cosi fermo lo spostmanto dei bg
        stop();
    }

    void rightCollisionDetected() {
        cantMooveRight = true;
        stop();
    }

    public void die() {
        if (!this.dead) {
            this.dead = true;
            setSpeedY(-20);

            rUpCollision.setLocation((int) rUpCollision.getLocation().getX(), (int) (rUpCollision.getLocation().getY() - (rUpCollision.getLocation().getY() % 32)));
            this.centerY = (int) rUpCollision.getLocation().getY() + 16;
        }
    }

    //GETTER
    boolean getJump() {
        return this.jumped;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public boolean isJumped() {
        return jumped;
    }

    public boolean isDucked() {
        return this.ducked;
    }

    public boolean isMoving() {
        return (movingLeft || movingRight);
    }

    public static Rectangle getrSurroundingMario() {
        return rSurroundingMario;
    }

    public int getJumpX() {
        return jumpX;
    }

    public int getJumpY() {
        return jumpY;
    }

    public boolean isTurnedRight() {
        return turnedRight;
    }

    public int getJUMPSPEED() {
        return JUMPSPEED;
    }

    public int getMOVESPEED() {
        return MOVESPEED;
    }

    public static Background getBg1() {
        return bg1;
    }

    public static Background getBg2() {
        return bg2;
    }

    public static Rectangle getrUpCollision() {
        return rUpCollision;
    }

    public static Rectangle getrDownCollision() {
        return rDownCollision;
    }

    public static Rectangle getrLeftCollision() {
        return rLeftCollision;
    }

    public static Rectangle getrRightCollision() {
        return rRightCollision;
    }

    public boolean isMovingLeft() {
        return movingLeft;
    }

    public boolean isMovingRight() {
        return movingRight;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public boolean isCantMooveLeft() {
        return cantMooveLeft;
    }

    public boolean isCantMooveRight() {
        return cantMooveRight;
    }

    public boolean isDead() {
        return this.dead;
    }

    //SETTER
    void setDuck(boolean isDucked) {
        this.ducked = isDucked;
        this.stop();
    }

    public static void setBg1(Background bg1) {
        Mario.bg1 = bg1;
    }

    public static void setBg2(Background bg2) {
        Mario.bg2 = bg2;
    }

    public static void setrUpCollision(Rectangle rUpCollision) {
        Mario.rUpCollision = rUpCollision;
    }

    public static void setrDownCollision(Rectangle rDownCollision) {
        Mario.rDownCollision = rDownCollision;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public void setMovingLeft(boolean movingLeft) {
        this.movingLeft = movingLeft;
    }

    public void setMovingRight(boolean movingRight) {
        this.movingRight = movingRight;
    }

    public void setDucked(boolean ducked) {
        this.ducked = ducked;
    }

    public void setTurnedRight(boolean turnedRight) {
        this.turnedRight = turnedRight;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    void setJump(boolean b) {
        this.jumped = b;
    }

    public void setCantMooveLeft(boolean cantMooveLeft) {
        this.cantMooveLeft = cantMooveLeft;
    }

    public void setCantMooveRight(boolean cantMooveRight) {
        this.cantMooveRight = cantMooveRight;
    }

    void enemyHit() {
        this.speedY = -this.speedY - 1;
    }
}
